<style>
    html, body {
        height: 100%;
    }

    body {
        background-image: url("assets/images/home_bg.jpeg");
    }
</style>

<!--
<div class="container fill">
    <div class="row justify-content-md-center align-items-center">
        <div class="col col-lg-6 mt-5 mb-auto text-center">
            <div class="jumbotron bg-transparent bg-warning animate__animated animate__flipInX">
            <h1 class="display-4">La paz en las colmenas</h1>
            <h3>Acompaña a Eirene en esta aventura.</h3>
            <p class="lead">
                <a class="btn btn-light btn-lg" href="#" role="button">Vamos</a>
            </p>
            </div>

        </div>
    </div>

    <div class="row justify-content-lg-end">
        <div class="col col-lg-3">
            <img class="animate__animated animate__pulse animate__infinite" style="height: 350px"
                 src="assets/images/eirene.png">
        </div>
    </div>
</div>
-->
<div class="container-fluid h-100">
    <div class="row mt-5 mb-5">
        <div class="col text-center mt-5">
            <h1 class="display-3 mt-5 animate__animated animate__zoomInDown">
                <b>¡Sigue el camino de las colmenas!</b>
            </h1>
        </div>
    </div>
    <div class="row justify-content-center align-items-center mt-5 text-center">
        <div class="col mt-5">
            <a href="?c=peace">
                <img class="animate__animated animate__pulse animate__infinite" style="max-height: 200px"
                     src="assets/images/hive.png">
            </a>
            <h1 class="display-4">
                <b>Colmena Paz</b>
            </h1>
        </div>
        <div class="col mt-5">
            <a href="?c=opinions">
            <img class="animate__animated animate__pulse animate__infinite" style="max-height: 200px"
                 src="assets/images/hive.png">
            </a>
            <h1 class="display-4">
                <b>Las niñas y los niños opinamos</b>
            </h1>
        </div>
        <div class="col mt-5">
            <a href="?c=maker">
            <img class="animate__animated animate__pulse animate__infinite" style="max-height: 200px"
                 src="assets/images/hive.png">
            </a>
            <h1 class="display-4">
                <b>Colmena Maker</b>
            </h1>
        </div>
    </div>
    <div class="row justify-content-end align-items-center mt-5 text-center">
        <div class="col col-lg-3 mt-5 mb-5">
            <img class="animate__animated animate__pulse animate__infinite" style="max-height: 150px"
                 src="assets/images/vase.png">
            <h1>
                Para saber más
            </h1>
        </div>
    </div>
</div>

<style>
    html, body {
        height: 100%;
    }

    body {
        background-image: url("assets/images/hive_bg.svg");
    }
</style>

<div class="sticky-top">
    <a href="?c=home">
    <span class="fa-stack fa-2x m-2">
        <i class="fas fa-square fa-stack-2x text-danger"></i>
        <i class="fas fa-arrow-left fa-stack-1x fa-inverse"></i>
    </span>
    </a>
    <a href="?c=home">
    <span class="fa-stack fa-2x">
        <i class="fas fa-square fa-stack-2x text-info"></i>
        <i class="fas fa-home fa-stack-1x fa-inverse"></i>
    </span>
    </a>
</div>

<div class="container">
    <div class="row  mt-5">
        <div class="col col-lg-8">
            <div class="jumbotron animate__animated animate__flipInX">
                <h1 class="display-4 animate__animated animate__zoomInDown animate__delay-1s">
                    <span class="badge badge-primary">Colmena Paz</span>
                </h1>
                <p class="lead animate__animated animate__flipInY animate__delay-1s">
                    Aquí se desarrolla la vida de Eirene. En donde viven saben que la paz es importante para su
                    comunidad.
                </p>
                <p class="lead animate__animated animate__flipInY animate__delay-1s">
                    Observa todo lo que ven y hacen en su colmena llamada "Paz"
                </p>
            </div>
        </div>
        <div class="col text-center">
            <img class="animate__animated animate__pulse animate__infinite" style="height: 350px"
                 src="assets/images/eirene.png">
        </div>
    </div>
    <div class="row text-center mt-5">
        <div class="col">
            <a href="?c=peace">
                <img class="bg-warning border border-white rounded p-3 animate__animated animate__pulse animate__infinite"
                     style="max-height: 200px"
                     src="assets/images/peace_1.png">
            </a>
        </div>
        <div class="col">
            <a href="?c=peace">
                <img class="bg-warning border border-white rounded p-3 animate__animated animate__pulse animate__infinite"
                     style="max-height: 200px"
                     src="assets/images/peace_2.png">
            </a>
        </div>
        <div class="col">
            <a href="?c=peace">
                <img class="bg-warning border border-white rounded p-3 animate__animated animate__pulse animate__infinite"
                     style="max-height: 200px"
                     src="assets/images/peace_3.png">
            </a>
        </div>
        <div class="col">
            <a href="?c=peace&a=liberty">
                <img class="bg-warning border border-white rounded p-3 animate__animated animate__pulse animate__infinite"
                     style="max-height: 200px"
                     src="assets/images/peace_4.png">
            </a>
        </div>
    </div>
    <div class="row mt-5 mb-5 text-center">
        <div class="col">
            <a href="?c=peace">
                <img class="bg-warning border border-white rounded p-3 animate__animated animate__pulse animate__infinite"
                     style="max-height: 200px"
                     src="assets/images/peace_5.png">
            </a>
        </div>
        <div class="col">
            <a href="?c=peace">
                <img class="bg-warning border border-white rounded p-3 animate__animated animate__pulse animate__infinite"
                     style="max-height: 200px"
                     src="assets/images/peace_6.png">
            </a>
        </div>
        <div class="col">
            <a href="?c=peace">
                <img class="bg-warning border border-white rounded p-3 animate__animated animate__pulse animate__infinite"
                     style="max-height: 200px"
                     src="assets/images/peace_7.png">
            </a>
        </div>
        <div class="col">
            <a href="?c=peace">
                <img class="bg-warning border border-white rounded p-3 animate__animated animate__pulse animate__infinite"
                     style="max-height: 200px"
                     src="assets/images/peace_8.png">
            </a>
        </div>
    </div>
</div>

<style>
    html, body {
        height: 100%;
    }

    .fill {
        height: 100%;
    }

    body {
        background-image: url("assets/images/pixel_stars.png"), linear-gradient(43deg, #c557b4 0%, #f736a6 46%, #ff7d6c 100%);
    }
</style>

<div class="container h-100">
    <div class="row h-100 justify-content-center align-items-center">
        <div class="col col-lg-8  text-center">
            <div class="jumbotron bg-warning animate__animated animate__flipInX">
                <h1 class="display-4 animate__animated animate__zoomInDown animate__delay-1s">
                    La paz en las colmenas
                </h1>
                <h3 class="animate__animated animate__zoomInDown animate__delay-1s">
                    Acompaña a Eirene en esta aventura.
                </h3>
                <br>
                <p class="animate__animated animate__backInDown animate__delay-2s">
                    <a class="btn btn-block btn-light btn-lg" href="?c=home" role="button">¡Vamos!</a>
                </p>
            </div>
        </div>
        <div class="col  text-center">

            <img class="animate__animated animate__pulse animate__infinite" style="height: 350px"
                 src="assets/images/eirene.png">
        </div>
    </div>
</div>

<style>
    body {
        background-image: url("assets/images/hive_bg.svg");
    }
</style>

<div class="sticky-top">
    <a href="?c=home">
    <span class="fa-stack fa-2x m-2">
        <i class="fas fa-square fa-stack-2x text-danger"></i>
        <i class="fas fa-arrow-left fa-stack-1x fa-inverse"></i>
    </span>
    </a>
    <a href="?c=home">
    <span class="fa-stack fa-2x">
        <i class="fas fa-square fa-stack-2x text-info"></i>
        <i class="fas fa-home fa-stack-1x fa-inverse"></i>
    </span>
    </a>
</div>

<div class="container">
    <div class="row  mt-5">
        <div class="col col-lg-8">
            <div class="jumbotron animate__animated animate__flipInX">
                <h1 class="display-4 animate__animated animate__zoomInDown animate__delay-1s">
                    <span class="badge badge-success">Colmena Maker</span>
                </h1>
                <p class="lead animate__animated animate__flipInY animate__delay-1s">
                    Serás quien cree el contenido para divulgarlo a las colmenas de tu comunidad.
                    <br>
                    Revisa:
                </p>
                <ol class="lead">
                    <li>Foco de inspiración</li>
                    <li>La libreta del manual de uso</li>
                    <li>Lapiz de crear tu cómic</li>
                    <li>Sube tu cómic</li>
                    <li>Observa a la comunidad</li>
                </ol>
            </div>
        </div>
        <div class="col text-center">
            <h2><span class="badge badge-secondary">Éxito</span></h2>
            <img class="animate__animated animate__pulse animate__infinite" style="height: 350px"
                 src="assets/images/eirene.png">

        </div>
    </div>
    <div class="row text-center mt-5">
        <div class="col">
            <a href="?c=opinions&a=kids1">
                <img class="bg-info border border-white rounded p-3 animate__animated animate__pulse animate__infinite"
                     style="max-height: 200px; max-width: 200px"
                     src="assets/images/kids_1.jpeg">
            </a>
        </div>
        <div class="col">
            <a href="?c=opinions">
                <img class="bg-info border border-white rounded p-3 animate__animated animate__pulse animate__infinite"
                     style="max-height: 200px; max-width: 200px"
                     src="assets/images/kids_1.jpeg">
            </a>
        </div>
        <div class="col">
            <a href="?c=opinions">
                <img class="bg-info border border-white rounded p-3 animate__animated animate__pulse animate__infinite"
                     style="max-height: 200px; max-width: 200px"
                     src="assets/images/kids_1.jpeg">
            </a>
        </div>
        <div class="col">
            <a href="?c=opinions">
                <img class="bg-info border border-white rounded p-3 animate__animated animate__pulse animate__infinite"
                     style="max-height: 200px; max-width: 200px"
                     src="assets/images/kids_1.jpeg">
            </a>
        </div>

    </div>
</div>

<style>
    body {
        background-image: url("assets/images/hive_bg.svg");
    }
</style>

<div class="sticky-top">
    <a href="?c=opinions">
    <span class="fa-stack fa-2x m-2">
        <i class="fas fa-square fa-stack-2x text-danger"></i>
        <i class="fas fa-arrow-left fa-stack-1x fa-inverse"></i>
    </span>
    </a>
    <a href="?c=home">
    <span class="fa-stack fa-2x">
        <i class="fas fa-square fa-stack-2x text-info"></i>
        <i class="fas fa-home fa-stack-1x fa-inverse"></i>
    </span>
    </a>
</div>

<div class="container">
    <div class="row mt-5 justify-content-center">
        <div class="col col-lg-8 ">
            <h1 class="display-4 animate__animated animate__zoomInDown animate__delay-1s">
                <span class="badge badge-info">Justicia</span>
            </h1>
            <div class="embed-responsive embed-responsive-16by9 mt-3">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/zpOULjyy-n8?rel=0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>

<div class="fixed-bottom">
    <a href="?c=home" class="float-right">
    <span class="fa-stack fa-2x m-2">
        <i class="fas fa-square fa-stack-2x text-success"></i>
        <i class="fas fa-arrow-right fa-stack-1x fa-inverse"></i>
    </span>
    </a>
</div>

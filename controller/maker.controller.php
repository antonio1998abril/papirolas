<?php

class MakerController{
    public function Index(){
        require_once 'view/header.php';
        require_once 'view/maker/index.php';
        require_once 'view/footer.php';
    }

    public function Kids1(){
        require_once 'view/header.php';
        require_once 'view/opinions/kids1.php';
        require_once 'view/footer.php';
    }
}
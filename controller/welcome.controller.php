<?php

class WelcomeController{
    
    public function Index(){
        require_once 'view/header.php';
        require_once 'view/welcome/index.php';
        require_once 'view/footer.php';
    }
}
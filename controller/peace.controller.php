<?php

class PeaceController{
    public function Index(){
        require_once 'view/header.php';
        require_once 'view/peace/index.php';
        require_once 'view/footer.php';
    }

    public function Liberty(){
        require_once 'view/header.php';
        require_once 'view/peace/liberty.php';
        require_once 'view/footer.php';
    }
}